#!/bin/bash
#$ -M aanastas@nd.edu
#$ -m abe
#$ -r n
#$ -q *@@nlp-gpu
#$ -N mb-multisource
#$ -l gpu_card=1

modeltype=speechtextsource_tied

# Paths to directories for outputs
modeldir=/afs/crc.nd.edu/group/nlp/03/aanastas/dynet_models/multisource/mboshi
outputdir=/afs/crc.nd.edu/group/nlp/03/aanastas/dynet_outputs/multisource/mboshi
mkdir -p $modeldir
mkdir -p $outputdir

# Path to Data
data=/afs/crc.nd.edu/group/nlp/data/mboshi/mboshi-french-parallel-corpus-master/full_corpus_newsplit/train/
speechdata=$data/plp/
mbdata=$data/mbclsp/
frdata=$data/frcl/

trainfiles=$data/train_files.txt
devfiles=$data/dev_files.txt

# Path to test data
testdata=/afs/crc.nd.edu/group/nlp/data/mboshi/mboshi-french-parallel-corpus-master/full_corpus_newsplit/dev
testfiles=$testdata/test_files.txt
testspeechdata=$testdata/plp/
testfrdata=$testdata/frcl/

# File extensions
speechend=.plp
txt1end=.fr.cleaned
txt2end=.mb.cleaned.split

# Needed as parameters depending on whether the text input and output
# is at the character level, or at the word level.
# Options:
# --input_char or --input_word
# --output_char or --output_word
details="--input_char --output_char"

# Logging parameters
l1=mbs
l2=frw
l3=mbc
size=512
layers=1
dropout=0.2
desc=$size-$layers-$layers-$dropout
log=log.txt
echo "enc: bidir compress; dec1: $layers; dec2: $layers; dropout: $dropout; adam: lr 0.002; state_size = rep_size = $size; batch size = 1" > $modeldir/$modeltype-$desc-$l1$l2$l3.info

# Training
./examples/train_multisourcemodels-speech $modeltype $trainfiles $devfiles --speech_dir $speechdata --text_dir1 $frdata --text_dir2 $mbdata --out $modeldir/$modeltype-$desc-$l1$l2$l3 --layers $layers --size $size --dropout $dropout --epochs 250 --batchsize 8 --speech_end $speechend --text1_end $txt1end --text2_end $txt2end $details &> $log

# Testing
modelin=$modeldir/$modeltype-$desc-$l1$l2$l3.best.params
output=$outputdir/$l1$l2$l3.test.$modeltype.$desc.best
./examples/train_multisourcemodels-speech $modeltype $trainfiles $devfiles --speech_dir $speechdata --text_dir1 $frdata --text_dir2 $mbdata --in $modelin --test $testfiles --test_speech_dir $testspeechdata --test_text_dir1 $testfrdata --layers $layers --size $size --speech_end $speechend --text1_end $txt1end --text2_end $txt2end $details > $output

# Force decode and print attention weights
attentiondir=/afs/crc.nd.edu/group/nlp/03/aanastas/dynet_attentions/multisource/mboshi
mkdir -p $attentiondir
output=$attentiondir/$l1$l2$l3.test.$modeltype.$desc.best.attentions
./examples/train_multisourcemodels-speech $modeltype $trainfiles $devfiles --speech_dir $speechdata --text_dir1 $frdata --text_dir2 $mbdata --in $modelin --test $devfiles --test_speech_dir $speechdata --test_text_dir1 $frdata --test_text_dir2 $mbdata --layers $layers --size $size --speech_end $speechend --text1_end $txt1end --text2_end $txt2end $details --attention > $output

