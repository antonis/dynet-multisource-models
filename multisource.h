/*
 * cascade.h
 */

#ifndef EXAMPLES_CPP_MULTISOURCE_H_
#define EXAMPLES_CPP_MULTISOURCE_H_

//#include "attention.h"
#include "dynet/dynet.h"
#include "dynet/training.h"
#include "dynet/lstm.h"
using namespace std;
using namespace dynet;

class multisource {
public:

  virtual void initialize(ParameterCollection& model) = 0;

  virtual Expression get_loss(const vector<vector<float>>&  input_sentence, const vector<int>&  int_sentence, const vector<int>&  output_sentence, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, 
      LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm, LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, ComputationGraph& cg) = 0;
  virtual Expression get_loss(const vector<vector<vector<float>>>&  input_sentence, const vector<vector<int>>&  int_sentence, const vector<vector<int>>&  output_sentence, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, 
      LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm, LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, ComputationGraph& cg) = 0;


  virtual float train(ParameterCollection& model, const vector<vector<vector<float>>>&  in_seq, const vector<vector<int>>&  int_seq,  const vector<vector<int>>&  out_seq, AdamTrainer& trainer, dynet::real l_scale) = 0;
  virtual float test_dev(ParameterCollection& model, const vector<vector<float>>&  in_seq, const vector<int>&  int_seq, const vector<int>&  out_seq) = 0;
  virtual void test(ParameterCollection& model, const vector<vector<float>>&  in_seq, const vector<int>& int_seq, int beamsize) = 0;

  virtual Expression decode(LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, vector<Expression>& encoded_speech, vector<Expression>& encoded_sent, const vector<int>& trg_sentence, ComputationGraph& cg) = 0;
  virtual Expression decode(LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, vector<Expression>& encoded_speech, vector<Expression>& encoded_sent, const vector<vector<int>>& trg_batch, ComputationGraph& cg) = 0;

  virtual void dump_attentions(ParameterCollection& model, const vector<vector<float>>& in_seq, const vector<int>&  int_seq, const vector<int>&  out_seq) = 0;

  virtual Expression attend_1(LSTMBuilder& state, Expression w1dt, ComputationGraph& cg) = 0;

  vector<Expression> encode_sentence(LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, vector<Expression>& embedded);
  vector<Expression> encode_features(LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, vector<Expression>& embedded);

  vector<Expression> run_lstm(LSTMBuilder& init_state, const vector<Expression>& input_vecs);

  vector<Expression> embed_sentence(const vector<int>&  sentence, ComputationGraph& cg);
  vector<Expression> embed_sentence(const vector<vector<int>>&  batch, ComputationGraph& cg);
  vector<Expression> embed_features(const vector<vector<float>>&  sentence, ComputationGraph& cg);
  vector<Expression> embed_features(const vector<vector<vector<float>>>&  batch, ComputationGraph& cg);
  vector<Expression> embed_stack_features(const vector<vector<float>>&  sentence, ComputationGraph& cg);
  vector<Expression> embed_stack_features(const vector<vector<vector<float>>>&  batch, ComputationGraph& cg);

  float bleu(const vector<int>& hyp, const vector<int>& ref);
  map<vector<int>,int> get_ngrams(const vector<int>& sentence);


//private:
  static const int REP_SIZE = 512;
  Parameter decoder_1_w;
  Parameter decoder_1_b;

  Parameter decoder_2_w;
  Parameter decoder_2_b;


  Parameter attention_1_w1;
  Parameter attention_1_w2;
  Parameter attention_1_v;

  Parameter attention_2_w1;
  Parameter attention_2_w2;
  Parameter attention_2_v;

  LookupParameter output_1_lookup;
};

class speechtextsource : public multisource
{
public:
  void initialize(ParameterCollection& model);

  Expression get_loss(const vector<vector<float>>&  input_sentence, const vector<int>&  int_sentence, const vector<int>&  output_sentence, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm, LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, ComputationGraph& cg){
    return get_loss(input_sentence, int_sentence, output_sentence, enc_fwd_lstm, enc_bwd_lstm, enc_compress_lstm1, enc_compress_lstm2, enc_fwd_w_lstm, enc_bwd_w_lstm, dec_1_lstm, cg);
  };
  Expression get_loss(const vector<vector<vector<float>>>&  input_sentence, const vector<vector<int>>&  int_sentence, const vector<vector<int>>&  output_sentence, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, 
    LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm, LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, ComputationGraph& cg){
    return get_loss(input_sentence, int_sentence, output_sentence, enc_fwd_lstm, enc_bwd_lstm, enc_compress_lstm1, enc_compress_lstm2, enc_fwd_w_lstm, enc_bwd_w_lstm, dec_1_lstm, cg);
  };
  Expression get_loss(const vector<vector<float>>&  input_sentence, const vector<int>&  int_sentence, const vector<int>&  output_sentence, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm, LSTMBuilder& dec_1_lstm, ComputationGraph& cg);
  Expression get_loss(const vector<vector<vector<float>>>&  input_sentence, const vector<vector<int>>&  int_sentence, const vector<vector<int>>&  output_sentence, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, 
    LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm, LSTMBuilder& dec_1_lstm, ComputationGraph& cg); 

  float train(ParameterCollection& model, const vector<vector<vector<float>>>&  in_seq, const vector<vector<int>>&  int_seq,  const vector<vector<int>>&  out_seq, AdamTrainer& trainer, dynet::real l_scale);
  float test_dev(ParameterCollection& model, const vector<vector<float>>&  in_seq, const vector<int>&  int_seq, const vector<int>&  out_seq);
  float test_dev_bleu(ParameterCollection& model, const vector<vector<float>>&  in_seq, const vector<int>&  int_seq, const vector<int>&  out_seq);
  void test(ParameterCollection& model, const vector<vector<float>>&  in_seq, const vector<int>& int_sentence, int beamsize);
  vector<int> generate_nbest(const vector<vector<float>>&  in_seq, const vector<int>& int_sentence, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm, LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, unsigned nbest_1_size, int beamsize){
    return generate_nbest(in_seq, int_sentence, enc_fwd_lstm, enc_bwd_lstm, enc_compress_lstm1, enc_compress_lstm2, enc_fwd_w_lstm, enc_bwd_w_lstm, dec_1_lstm, nbest_1_size, beamsize);
  };
  vector<int> generate_nbest(const vector<vector<float>>&  in_seq, const vector<int>& int_sentence, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm, LSTMBuilder& dec_1_lstm, unsigned nbest_1_size, int beamsize);
  
  Expression decode(LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, vector<Expression>& encoded_speech, vector<Expression>& encoded_sent, const vector<int>& trg_sentence, ComputationGraph& cg){
    return decode(dec_1_lstm, encoded_speech, encoded_sent, trg_sentence, cg);
  };
  Expression decode(LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, vector<Expression>& encoded_speech, vector<Expression>& encoded_sent,  const vector<vector<int>>& trg_batch, ComputationGraph& cg){
    return decode(dec_1_lstm, encoded_speech, encoded_sent, trg_batch, cg);
  };
  Expression decode(LSTMBuilder& dec_1_lstm, vector<Expression>& encoded_speech, vector<Expression>& encoded_sent, const vector<int>& trg_sentence, ComputationGraph& cg);
  Expression decode(LSTMBuilder& dec_1_lstm, vector<Expression>& encoded_speech, vector<Expression>& encoded_sent,  const vector<vector<int>>& trg_batch, ComputationGraph& cg);
  
  
  Expression attend_1(LSTMBuilder& state, Expression w1dt, ComputationGraph& cg);
  //Expression attend_2(LSTMBuilder& state, Expression w1dt, ComputationGraph& cg);
  Expression attend_2(LSTMBuilder& state, Expression w1dt, ComputationGraph& cg);

  void dump_attentions(ParameterCollection& model, const vector<vector<float>>& in_seq, const vector<int>&  int_seq, const vector<int>&  out_seq);
  void decode_attentions(LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, vector<Expression>& encoded_speech, vector<Expression>& encoded_sent, const vector<int>& trg_sentence, ComputationGraph& cg){
    return decode_attentions(dec_1_lstm, encoded_speech, encoded_sent, trg_sentence, cg);
  };
  void decode_attentions(LSTMBuilder& dec_1_lstm, vector<Expression>& encoded_speech, vector<Expression>& encoded_sent, const vector<int>& trg_sentence, ComputationGraph& cg);

};

class speechsource : public multisource
{
public:
  void initialize(ParameterCollection& model);

  Expression get_loss(const vector<vector<float>>&  input_sentence, const vector<int>&  int_sentence, const vector<int>&  output_sentence, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm, LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, ComputationGraph& cg){
    return get_loss(input_sentence, output_sentence, enc_fwd_lstm, enc_bwd_lstm, enc_compress_lstm1, enc_compress_lstm2, dec_1_lstm, cg);
  };
  Expression get_loss(const vector<vector<float>>&  input_sentence, const vector<int>&  output_sentence, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, LSTMBuilder& dec_1_lstm, ComputationGraph& cg);
  Expression get_loss(const vector<vector<vector<float>>>&  input_sentence, const vector<vector<int>>&  int_sentence, const vector<vector<int>>&  output_sentence, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, 
    LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm, LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, ComputationGraph& cg){
    return get_loss(input_sentence, output_sentence, enc_fwd_lstm, enc_bwd_lstm, enc_compress_lstm1, enc_compress_lstm2, dec_1_lstm, cg);
  };
  Expression get_loss(const vector<vector<vector<float>>>&  input_sentence, const vector<vector<int>>&  output_sentence, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, 
    LSTMBuilder& dec_1_lstm, ComputationGraph& cg);
  float train(ParameterCollection& model, const vector<vector<vector<float>>>&  in_seq, const vector<vector<int>>&  int_seq,  const vector<vector<int>>&  out_seq, AdamTrainer& trainer, dynet::real l_scale){
    return train(model, in_seq, out_seq, trainer, l_scale);
  };
  float train(ParameterCollection& model, const vector<vector<vector<float>>>&  in_seq, const vector<vector<int>>&  out_seq, AdamTrainer& trainer, dynet::real l_scale);
  float test_dev(ParameterCollection& model, const vector<vector<float>>&  in_seq, const vector<int>&  int_seq, const vector<int>&  out_seq){
    return test_dev(model, in_seq, out_seq);
  };
  float test_dev(ParameterCollection& model, const vector<vector<float>>&  in_seq, const vector<int>&  out_seq);
  float test_dev_bleu(ParameterCollection& model, const vector<vector<float>>&  in_seq, const vector<int>&  int_seq, const vector<int>&  out_seq){
    return test_dev_bleu(model, in_seq, out_seq);
  };
  float test_dev_bleu(ParameterCollection& model, const vector<vector<float>>&  in_seq, const vector<int>&  out_seq);
  void test(ParameterCollection& model, const vector<vector<float>>&  in_seq, const vector<int>& int_sentence, int beamsize){
    return test(model, in_seq, beamsize);
  };
  void test(ParameterCollection& model, const vector<vector<float>>&  in_seq, int beamsize);
  vector<int> generate_nbest(const vector<vector<float>>&  in_seq, const vector<int>& int_sentence, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm, LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, unsigned nbest_1_size, int beamsize){
    return generate_nbest(in_seq, enc_fwd_lstm, enc_bwd_lstm, enc_compress_lstm1, enc_compress_lstm2, dec_1_lstm, nbest_1_size, beamsize);
  };
  vector<int> generate_nbest(const vector<vector<float>>&  in_seq, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, LSTMBuilder& dec_1_lstm, unsigned nbest_1_size, int beamsize);
  
  Expression decode(LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, vector<Expression>& encoded_speech, vector<Expression>& encoded_sent, const vector<int>& trg_sentence, ComputationGraph& cg){
    return decode(dec_1_lstm, encoded_speech, trg_sentence, cg);
  };
  Expression decode(LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, vector<Expression>& encoded_speech, vector<Expression>& encoded_sent,  const vector<vector<int>>& trg_batch, ComputationGraph& cg){
    return decode(dec_1_lstm, encoded_speech, trg_batch, cg);
  };
  Expression decode(LSTMBuilder& dec_1_lstm, vector<Expression>& encoded_speech, const vector<int>& trg_sentence, ComputationGraph& cg);
  Expression decode(LSTMBuilder& dec_1_lstm, vector<Expression>& encoded_speech, const vector<vector<int>>& trg_batch, ComputationGraph& cg);
  
  
  Expression attend_1(LSTMBuilder& state, Expression w1dt, ComputationGraph& cg);

  void dump_attentions(ParameterCollection& model, const vector<vector<float>>& in_seq, const vector<int>&  int_seq, const vector<int>&  out_seq){
    return dump_attentions(model, in_seq, out_seq);
  };
  void dump_attentions(ParameterCollection& model, const vector<vector<float>>& in_seq, const vector<int>&  out_seq);
  void decode_attentions(LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, vector<Expression>& encoded_speech, const vector<int>& trg_sentence, ComputationGraph& cg){
    return decode_attentions(dec_1_lstm, encoded_speech, trg_sentence, cg);
  };
  void decode_attentions(LSTMBuilder& dec_1_lstm, vector<Expression>& encoded_speech, const vector<int>& trg_sentence, ComputationGraph& cg);

};

class textsource : public multisource
{
public:
  void initialize(ParameterCollection& model);

  Expression get_loss(const vector<vector<float>>&  input_sentence, const vector<int>&  int_sentence, const vector<int>&  output_sentence, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm, LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, ComputationGraph& cg){
    return get_loss(int_sentence, output_sentence, enc_fwd_w_lstm, enc_bwd_w_lstm, dec_1_lstm, cg);
  };
  Expression get_loss(const vector<int>&  int_sentence,  const vector<int>&  output_sentence, LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm, LSTMBuilder& dec_1_lstm, ComputationGraph& cg);
  Expression get_loss(const vector<vector<vector<float>>>&  input_sentence, const vector<vector<int>>&  int_sentence, const vector<vector<int>>&  output_sentence, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, 
    LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm, LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, ComputationGraph& cg){
    return get_loss(int_sentence, output_sentence, enc_fwd_w_lstm, enc_bwd_w_lstm, dec_1_lstm, cg);
  };
  Expression get_loss(const vector<vector<int>>&  int_sentence, const vector<vector<int>>&  output_sentence, LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm,
    LSTMBuilder& dec_1_lstm, ComputationGraph& cg);
  float train(ParameterCollection& model, const vector<vector<vector<float>>>&  in_seq, const vector<vector<int>>&  int_seq,  const vector<vector<int>>&  out_seq, AdamTrainer& trainer, dynet::real l_scale){
    return train(model, int_seq, out_seq, trainer, l_scale);
  };
  float train(ParameterCollection& model, const vector<vector<int>>&  int_seq, const vector<vector<int>>&  out_seq, AdamTrainer& trainer, dynet::real l_scale);
  float test_dev(ParameterCollection& model, const vector<vector<float>>&  in_seq, const vector<int>&  int_seq, const vector<int>&  out_seq){
    return test_dev(model, int_seq, out_seq);
  };
  float test_dev(ParameterCollection& model, const vector<int>&  int_seq, const vector<int>&  out_seq);
  float test_dev_bleu(ParameterCollection& model, const vector<vector<float>>&  in_seq, const vector<int>&  int_seq, const vector<int>&  out_seq){
    return test_dev_bleu(model, int_seq, out_seq);
  };
  float test_dev_bleu(ParameterCollection& model, const vector<int>&  int_seq, const vector<int>&  out_seq);
  void test(ParameterCollection& model, const vector<vector<float>>&  in_seq, const vector<int>& int_sentence, int beamsize){
    return test(model, int_sentence, beamsize);
  };
  void test(ParameterCollection& model, const vector<int>&  int_seq, int beamsize);
  vector<int> generate_nbest(const vector<vector<float>>&  in_seq, const vector<int>& int_sentence, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm, LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, unsigned nbest_1_size, int beamsize){
    return generate_nbest(int_sentence, enc_fwd_w_lstm, enc_bwd_w_lstm, dec_1_lstm, nbest_1_size, beamsize);
  };
  vector<int> generate_nbest(const vector<int>& int_sentence, LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm, LSTMBuilder& dec_1_lstm, unsigned nbest_1_size, int beamsize);
  
  Expression decode(LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, vector<Expression>& encoded_speech, vector<Expression>& encoded_sent, const vector<int>& trg_sentence, ComputationGraph& cg){
    return decode(dec_1_lstm, encoded_sent, trg_sentence, cg);
  };
  Expression decode(LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, vector<Expression>& encoded_speech, vector<Expression>& encoded_sent,  const vector<vector<int>>& trg_batch, ComputationGraph& cg){
    return decode(dec_1_lstm, encoded_sent, trg_batch, cg);
  };
  Expression decode(LSTMBuilder& dec_1_lstm, vector<Expression>& encoded_sent, const vector<int>& trg_sentence, ComputationGraph& cg);
  Expression decode(LSTMBuilder& dec_1_lstm, vector<Expression>& encoded_sent, const vector<vector<int>>& trg_batch, ComputationGraph& cg);
  
  
  Expression attend_1(LSTMBuilder& state, Expression w1dt, ComputationGraph& cg);

  void dump_attentions(ParameterCollection& model, const vector<vector<float>>& in_seq, const vector<int>&  int_seq, const vector<int>&  out_seq){
    return dump_attentions(model, int_seq, out_seq);
  };
  void dump_attentions(ParameterCollection& model, const vector<int>& int_seq, const vector<int>&  out_seq);
  void decode_attentions(LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, vector<Expression>& encoded_sent, const vector<int>& trg_sentence, ComputationGraph& cg){
    return decode_attentions(dec_1_lstm, encoded_sent, trg_sentence, cg);
  };
  void decode_attentions(LSTMBuilder& dec_1_lstm, vector<Expression>& encoded_sent, const vector<int>& trg_sentence, ComputationGraph& cg);

};

class speechtextsource_ensemble : public multisource
{
public:
  void initialize(ParameterCollection& model);

  Expression get_loss(const vector<vector<float>>&  input_sentence, const vector<int>&  int_sentence, const vector<int>&  output_sentence, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm, LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, ComputationGraph& cg);
  Expression get_loss(const vector<vector<vector<float>>>&  input_sentence, const vector<vector<int>>&  int_sentence, const vector<vector<int>>&  output_sentence, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, 
    LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm, LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, ComputationGraph& cg);

  float train(ParameterCollection& model, const vector<vector<vector<float>>>&  in_seq, const vector<vector<int>>&  int_seq,  const vector<vector<int>>&  out_seq, AdamTrainer& trainer, dynet::real l_scale);
  float test_dev(ParameterCollection& model, const vector<vector<float>>&  in_seq, const vector<int>&  int_seq, const vector<int>&  out_seq);
  float test_dev_bleu(ParameterCollection& model, const vector<vector<float>>&  in_seq, const vector<int>&  int_seq, const vector<int>&  out_seq);
  void test(ParameterCollection& model, const vector<vector<float>>&  in_seq, const vector<int>& int_sentence, int beamsize);
  vector<int> generate_nbest(const vector<vector<float>>&  in_seq, const vector<int>& int_sentence, LSTMBuilder& enc_fwd_lstm, LSTMBuilder& enc_bwd_lstm, LSTMBuilder& enc_compress_lstm1, LSTMBuilder& enc_compress_lstm2, LSTMBuilder& enc_fwd_w_lstm, LSTMBuilder& enc_bwd_w_lstm, LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, unsigned nbest_1_size, int beamsize);
  
  Expression decode(LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, vector<Expression>& encoded_speech, vector<Expression>& encoded_sent, const vector<int>& trg_sentence, ComputationGraph& cg);
  Expression decode(LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, vector<Expression>& encoded_speech, vector<Expression>& encoded_sent,  const vector<vector<int>>& trg_batch, ComputationGraph& cg);
    
  Expression attend_1(LSTMBuilder& state, Expression w1dt, ComputationGraph& cg);
  Expression attend_2(LSTMBuilder& state, Expression w1dt, ComputationGraph& cg);

  void dump_attentions(ParameterCollection& model, const vector<vector<float>>& in_seq, const vector<int>&  int_seq, const vector<int>&  out_seq);
  void decode_attentions(LSTMBuilder& dec_1_lstm, LSTMBuilder& dec_2_lstm, vector<Expression>& encoded_speech, vector<Expression>& encoded_sent, const vector<int>& trg_sentence, ComputationGraph& cg);

};

class DecoderHyp {
public:
    DecoderHyp(float score, const vector<Expression>& states, const vector<int> & sent) :
        score_(score), states_(states), sent_(sent) { }

    float GetScore() const { return score_; }
    const vector<Expression>& GetStates() const { return states_; }
    const vector<int>& GetSentence() const { return sent_; }

protected:

    float score_;
    vector<Expression> states_;
    vector<int> sent_;

};

typedef std::shared_ptr<DecoderHyp> DecoderHypPtr;

inline bool operator<(const DecoderHypPtr & lhs, const DecoderHypPtr & rhs) {
  assert(lhs.get() != nullptr);
  assert(rhs.get() != nullptr);
  if(lhs->GetScore() != rhs->GetScore()) return lhs->GetScore() > rhs->GetScore();
  return lhs->GetSentence() < rhs->GetSentence();
}

class DecoderHyp2 {
public:
    DecoderHyp2(float score, const vector<Expression>& states_1, const vector<Expression>& states_2, const vector<int> & sent) :
        score_(score), states_1_(states_1), states_2_(states_2), sent_(sent) { }

    float GetScore() const { return score_; }
    const vector<Expression>& GetStates1() const { return states_1_; }
    const vector<Expression>& GetStates2() const { return states_2_; }
    const vector<int>& GetSentence() const { return sent_; }

protected:

    float score_;
    vector<Expression> states_1_;
    vector<Expression> states_2_;
    vector<int> sent_;

};

typedef std::shared_ptr<DecoderHyp2> DecoderHypPtr2;

inline bool operator<(const DecoderHypPtr2 & lhs, const DecoderHypPtr2 & rhs) {
  assert(lhs.get() != nullptr);
  assert(rhs.get() != nullptr);
  if(lhs->GetScore() != rhs->GetScore()) return lhs->GetScore() > rhs->GetScore();
  return lhs->GetSentence() < rhs->GetSentence();
}


#endif /* EXAMPLES_CPP_MULTISOURCE_H_ */
