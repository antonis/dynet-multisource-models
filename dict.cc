#include "dict.h"

#include <string>
#include <vector>
#include <sstream>

using namespace std;

namespace dynet {

std::vector<int> read_sentence(const std::string& line, Dict& sd) {
  std::istringstream in(line);
  std::string word;
  std::vector<int> res;
  while(in) {
    in >> word;
    if (!in || word.empty()) break;
    res.push_back(sd.convert(word));
  }
  return res;
}

std::vector<std::vector<float>> read_features(const std::string& line) {
  std::istringstream in(line);
  std::string word;
  std::vector<std::vector<float>> res;
  std::string l;
  while(getline(in, l)) {
    std::istringstream in2(l);
    std::vector<float> tr;
    for (unsigned i=0; i < 39; i++){
      in2 >> word;
      tr.push_back(stof(word));
    }
    res.push_back(tr);
  }
  return res;
}


void read_sentence_pair(const std::string& line, std::vector<int>& s, Dict& sd, std::vector<int>& t, Dict& td) {
  std::istringstream in(line);
  std::string word;
  std::string sep = "|||";
  Dict* d = &sd;
  std::vector<int>* v = &s;
  while(in) {
    in >> word;
    if (!in) break;
    if (word == sep) { d = &td; v = &t; continue; }
    v->push_back(d->convert(word));
  }
}

void read_sentence_triple(const std::string& line, std::vector<int>& s, Dict& sd, std::vector<int>& i, Dict& id, std::vector<int>& t, Dict& td) {
  std::istringstream in(line);
  std::string word;
  std::string sep = "|||";
  Dict* d = &sd;
  std::vector<int>* v = &s;
  int first = 1;
  while(in) {
    in >> word;
    if (!in) break;
    if (word == sep) { 
      if (first == 0){
        d = &td; v = &t; continue;   
      }
      if (first == 1) {
        d = &id; v = &i; first--; continue;   
      }
      
    }
    v->push_back(d->convert(word));
  }
}


} // namespace dynet

