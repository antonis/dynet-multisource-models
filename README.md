# Neural Multisource Sequence-to-sequence Models

Writen in C++. You need to have [DyNet](http://dynet.readthedocs.io/en/latest/) installed.

Replace the dict.[cc,h] files of your Dynet dist (dynet/dict.*) with the ones provided, in order to include
a function for reading in speech features.

Sample run script is `sample_run.sh`.

The models implemented are:

* `speechsource` : simple, single task enc-dec with a speech encoder and attention

* `textsource` : simple, single task enc-dec with a text encoder and attention

* `speechtextsource` : simple multi-source: two encoders, with two separate attention mechanisms

* `speechtextsource_tied` : multi-source: two encoders, with two tied attention mechanisms (sharing some parameters)

* `speechtextsource_for_tied` : multi-source: two encoders, with a shared attention mechanisms (sharing all parameters)

* `speechtextsource_ensemble` : multi-source coupled ensemble: two separate encoders and decoders, averaged right before the softmax, trained jointly

For more information look at our paper

Antonis Anastasopoulos and David Chiang, 2018. [Leveraging translations for speech-transcription in low-resource settings](https://arxiv.org/abs/1803.08991).



